/* eslint-disable import/no-named-as-default */
import React from 'react';
import PropTypes from 'prop-types';
import {
  Route,
  Switch,
} from 'react-router-dom';
import { ConnectedRouter } from 'react-router-redux';


import { MuiThemeProvider, createMuiTheme } from 'material-ui/styles';

import ListPage from '../pages/list-page';
import UserPage from '../pages/user-page';

import Settings from '../components/settings';
import Info from '../components/info';

const theme = createMuiTheme();

class App extends React.Component {
  render() {
    const { history } = this.props;
    return (
      <MuiThemeProvider theme={ theme }>
        <ConnectedRouter history={ history }>
          <div>
            <Settings />
            <Switch>
              <Route exact path="/" component={ ListPage } />
              <Route name="user" path="/user/:userId" component={ UserPage } />

              <Route render={ () => <h2>Page not found</h2> } />
            </Switch>
            <Info />
          </div>
        </ConnectedRouter>
      </MuiThemeProvider>
    );
  }
}

App.propTypes = {
  children: PropTypes.element,
  history: PropTypes.object,
};

export default App;
