import React from 'react';
import PropTypes from 'prop-types';
import { Provider } from 'react-redux';
import App from './app';

const Root = (props) => (
  <Provider store={ props.store }>
    <App history={ props.history } />
  </Provider>
);

Root.propTypes = {
  store: PropTypes.object.isRequired,
  history: PropTypes.object.isRequired,
};

export default Root;
