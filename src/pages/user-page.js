import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import { CircularProgress } from 'material-ui/Progress';

import fetchUser from '../actions/user-get';
import editUser from '../actions/user-edit';

import ShowForm from '../components/user-show';
import EditForm from '../components/user-edit';

class UserPage extends React.Component {

  componentWillMount() {
    this.props.fetchUser(this.props.itemId);
  }

  handleFormSubmit = (values) => {
      this.props.editUser(values);
  };

  render() {
    const { isAdmin, isLoading, isProcessing, item } = this.props;
    let content;

    if (isLoading) {
      content = (
        <CircularProgress size={50} />
      );
    } else if (!item) {
      content = (
        <h3>Error fetching data</h3>
      );
    } else if (!isAdmin) {
      content = (
        <ShowForm isLoading={ isLoading } item={ item } />
      );
    } else {
      content = (
        <EditForm
          isLoading={ isLoading }
          initialValues={ item }
          isProcessing={ isProcessing }
          onSubmit={ this.handleFormSubmit }
        />
      );
    }
    return (
      <div>
        <Link to="/">Назад</Link>
        <div>
          {content}
        </div>
      </div>
    );
  }
}

UserPage.propTypes = {
  fetchUser: PropTypes.func.isRequired,
  editUser: PropTypes.func.isRequired,

  isAdmin: PropTypes.bool.isRequired,
  isLoading: PropTypes.bool.isRequired,
  isProcessing: PropTypes.bool.isRequired,
  itemId: PropTypes.string.isRequired,
  item: PropTypes.object,
};

const mapStateToProps = (state, ownProps) => ({
  itemId: ownProps.match.params.userId,
  isAdmin: state.settings.isAdmin,
  item: state.item.data,
  isLoading: state.item.isLoading,
  isProcessing: state.item.isProcessing,
});

const mapDispatchToProps = {
  fetchUser,
  editUser,
};


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(UserPage));
