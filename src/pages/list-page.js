import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { withStyles } from 'material-ui/styles';
import { GridList, GridListTile, GridListTileBar } from 'material-ui/GridList';

import fetchUsers from '../actions/user-list';
import Pagination from '../components/pagination';

const styles = theme => ({
  wrapper: {
    width: 500,
  },
  container: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'flex-start',
    overflow: 'hidden',
    background: theme.palette.background.paper,
  },
  gridList: {},
});

class ListPage extends React.Component {

  componentWillMount() {
    this.props.fetchUsers();
  }

  onNextUrl = () => {
    if (this.props.nextUrl) {
      this.props.fetchUsers({ url: this.props.nextUrl });
    }
  };

  onPrevUrl = () => {
    if (this.props.prevUrl) {
      this.props.fetchUsers({ url: this.props.prevUrl });
    }
  };

  render() {


    const { classes, items, prevUrl, nextUrl } = this.props;

    const pagination = (
      <Pagination
        prevUrl={ prevUrl }
        nextUrl={ nextUrl }
        onNext={ this.onNextUrl }
        onPrev={ this.onPrevUrl }
      />
    );

    return (
      <div className={ classes.wrapper }>
        { pagination }

        <div className={ classes.container }>

          <GridList className={ classes.gridList }>
            { items.map(item => (
              <GridListTile key={ item.id }>
                <Link to={ `/user/${item.id}` } params={ { userId: item.id } }>
                  <img src={ item.avatarUrl } alt={ item.name } />
                  <GridListTileBar title={ item.name } />
                </Link>
              </GridListTile>
            )) }
          </GridList>
        </div>

        { pagination }
      </div>
    );
  }
}

ListPage.propTypes = {
  fetchUsers: PropTypes.func,
  isAdmin: PropTypes.bool.isRequired,
  classes: PropTypes.object.isRequired,
  items: PropTypes.arrayOf(Object),
  prevUrl: PropTypes.string,
  nextUrl: PropTypes.string,
};

const mapStateToProps = (state) => ({
  isAdmin: state.settings.isAdmin,
  items: state.list.items,
  nextUrl: state.list.nextUrl,
  prevUrl: state.list.prevUrl,
});

const mapDispatchToProps = {
  fetchUsers,
};


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(styles)(ListPage));
