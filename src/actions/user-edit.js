import { createAction } from 'redux-act';

import Api from '../service/api';
import {showInfoAction} from './info';

export const editUserStartAction = createAction('edit user start');
export const editUserSuccessAction = createAction('edit user success');
export const editUserFailureAction = createAction('edit user failure');

export default (data) => (dispatch) => {
  dispatch(editUserStartAction(data));

  Api.setUser(data)
    .then(response => {
      dispatch(editUserSuccessAction(response));
      dispatch(showInfoAction('Запись изменена'));
    })
    .catch(error => dispatch(editUserFailureAction(error)));
};
