import { createAction } from 'redux-act';

import Api from '../service/api';

export const fetchUserStartAction = createAction('get user start');
export const fetchUserSuccessAction = createAction('get user success');
export const fetchUserFailureAction = createAction('get user failure');

export default (id) => (dispatch) => {
  dispatch(fetchUserStartAction(id));

  Api.getUser(id)
    .then(response => dispatch(fetchUserSuccessAction(response)))
    .catch(error => dispatch(fetchUserFailureAction(error)));
};
