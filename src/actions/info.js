import { createAction } from 'redux-act';

export const showInfoAction = createAction('showInfo');
export const hideInfoAction = createAction('hideInfo');
