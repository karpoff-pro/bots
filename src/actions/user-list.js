import { createAction } from 'redux-act';

import Api from '../service/api';

export const fetchUserStartAction = createAction('fetch users start');
export const fetchUserSuccessAction = createAction('fetch users success');
export const fetchUserFailureAction = createAction('fetch users failure');

export default ({ url = 'index' } = {}) => (dispatch) => {
  dispatch(fetchUserStartAction());

  Api.getUsers(url)
    .then(response => dispatch(fetchUserSuccessAction(response)))
    .catch(error => dispatch(fetchUserFailureAction(error)));
};
