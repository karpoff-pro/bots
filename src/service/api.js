const users = Object.create(null);

const Api = {
  getUsers: (url = 'index') => {
    const out = {
      result:
        [1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map(id => users[`${id}_${url}`] || ({
          id: `${id}_${url}`,
          name: `${id}_${url}`,
          avatarUrl: 'https://lumiere-a.akamaihd.net/v1/images/open-uri20150422-20810-2unaal_3530efaa.jpeg?region=0%2C0%2C600%2C600&width=300'
        })),
    };

    if (url === 'index') {
      out.nextPageUrl = 'next';
      out.previousPageUrl = 'prev';
    } else if (url === 'next') {
      out.previousPageUrl = 'index';
    } else if (url === 'prev') {
      out.nextPageUrl = 'index';
    }
    return Promise.resolve(out);
  },
  getUser: (id) => {
    return Promise.resolve(users[id] ? { result: users[id] } : {
      result: {
        id,
        name: id,
        avatarUrl: 'https://lumiere-a.akamaihd.net/v1/images/open-uri20150422-20810-2unaal_3530efaa.jpeg?region=0%2C0%2C600%2C600&width=300',
      },
    });
  },
  setUser: (data) => {
    users[data.id] = data;
    return Promise.resolve({});
  },
};

export default Api;
