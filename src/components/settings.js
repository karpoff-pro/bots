import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import IconButton from 'material-ui/IconButton';
import Menu, { MenuItem } from 'material-ui/Menu';
import MoreVertIcon from 'material-ui-icons/MoreVert';

import { setAdminAction } from '../actions/settings';

class Settings extends React.PureComponent {
  state = {
    anchorEl: null,
  };

  handleClick = event => {
    this.setState({ anchorEl: event.currentTarget });
  };

  handleRequestClose = () => {
    this.setState({ anchorEl: null });
  };

  toggleAdmin = () => {
    this.handleRequestClose();
    this.props.setAdminAction(!this.props.isAdmin);
  };

  render() {
    const open = Boolean(this.state.anchorEl);

    return (
      <div>
        <IconButton
          aria-label="More"
          aria-owns={ open ? 'long-menu' : null }
          aria-haspopup="true"
          onClick={ this.handleClick }
        >
          <MoreVertIcon />
        </IconButton>
        <Menu
          id="long-menu"
          anchorEl={ this.state.anchorEl }
          open={ open }
          onRequestClose={ this.handleRequestClose }
        >
          <MenuItem onClick={ this.toggleAdmin }>
            { this.props.isAdmin ? 'Я не админ' : 'Я админ' }
          </MenuItem>
        </Menu>
      </div>
    );
  }
}

Settings.propTypes = {
  setAdminAction: PropTypes.func.isRequired,
  isAdmin: PropTypes.bool.isRequired,
};


const mapStateToProps = (state) => ({
  isAdmin: state.settings.isAdmin,
});

const mapDispatchToProps = {
  setAdminAction,
};


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Settings);
