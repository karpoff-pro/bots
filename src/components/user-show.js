import React from 'react';
import { withStyles } from 'material-ui/styles';
import Card, { CardContent, CardMedia } from 'material-ui/Card';
import Typography from 'material-ui/Typography';
import PropTypes from 'prop-types';

const styles = {
  card: {
    maxWidth: 345,
  },
  media: {
    height: 200,
  },
};

const UserForm = (props) => (
  <Card className={ props.classes.card }>
    <CardMedia
      className={ props.classes.media }
      image={ props.item.avatarUrl }
    />
    <CardContent>
      <Typography type="headline" component="h2">
        { props.item.name }
      </Typography>
    </CardContent>
  </Card>
);

UserForm.propTypes = {
  item: PropTypes.object.isRequired,
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(UserForm);
