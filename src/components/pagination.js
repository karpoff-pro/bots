import React from 'react';
import IconButton from 'material-ui/IconButton';
import { NavigateBefore, NavigateNext } from 'material-ui-icons';
import PropTypes from 'prop-types';

const styles = {
  container: {
    width: '100%',
    display: 'flex',
    justifyContent: 'space-around',
  }
};
const Pagination = (props) => (
  <div style={ styles.container }>
    <IconButton disabled={ !props.prevUrl } onClick={ props.onPrev }>
      <NavigateBefore />
    </IconButton>
    <IconButton disabled={ !props.nextUrl } onClick={ props.onNext }>
      <NavigateNext />
    </IconButton>
  </div>
);

Pagination.propTypes = {
  onPrev: PropTypes.func,
  onNext: PropTypes.func,
  prevUrl: PropTypes.string,
  nextUrl: PropTypes.string,
};

export default Pagination;
