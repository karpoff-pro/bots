import React from 'react';
import { connect } from 'react-redux';
import { Snackbar } from 'material-ui';

import { hideInfoAction } from '../actions/info';
import PropTypes from 'prop-types';


const InfoMessage = (props) => (
  <Snackbar
    anchorOrigin={ { vertical: 'bottom', horizontal: 'center' } }
    open={ props.message !== '' }
    autoHideDuration={ 3000 }
    onRequestClose={ () => props.handleClear() }
    message={ <span>{ props.message }</span> }
  />
);

InfoMessage.propTypes = {
  message: PropTypes.string,
  handleClear: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  message: state.info.message,
});

const mapDispatchToProps = {
  handleClear: hideInfoAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(InfoMessage);
