import React from 'react';
import { Field, reduxForm } from 'redux-form';
import { TextField } from 'redux-form-material-ui';
import Button from 'material-ui/Button';
import PropTypes from 'prop-types';

const UserForm = (props) => {
  const { handleSubmit, onSubmit, isProcessing } = props;
  return (
    <form onSubmit={ handleSubmit(onSubmit) }>
      <Button
        raised
        type="submit"
        style={ { marginLeft: '10px', float: 'right' } }
        disabled={ isProcessing }
      >
        Сохранить
      </Button>

      <h1>Редактирование пользователя</h1>

      <Field
        name="name"
        component={ TextField }
        label="First Name"
        fullWidth
      />
      <Field
        name="avatarUrl"
        component={ TextField }
        label="Avatar"
        fullWidth
      />
    </form>
  );
};

UserForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  isProcessing: PropTypes.bool.isRequired,
};

export default reduxForm({
  form: 'user'
})(UserForm);
