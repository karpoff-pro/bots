import { createReducer } from 'redux-act';

import {
  fetchUserStartAction,
  fetchUserSuccessAction,
  fetchUserFailureAction,
} from '../actions/user-list';

const initialState = {
  items: [],
  isLoading: false,
  previousUrl: null,
  nextUrl: null,
};

const fetchSuccessReducer = (state, payload) => ({
  ...state,
  isLoading: false,
  items: payload.result,
  nextUrl: payload.nextPageUrl || null,
  prevUrl: payload.previousPageUrl || null,
});

const fetchStartReducer = (state) => ({
  ...state,
  isLoading: true,
});

const fetchFailureReducer = (state) => ({
  ...state,
  isLoading: false,
});

export default createReducer({
  [fetchUserStartAction]: fetchStartReducer,
  [fetchUserSuccessAction]: fetchSuccessReducer,
  [fetchUserFailureAction]: fetchFailureReducer,
}, initialState);
