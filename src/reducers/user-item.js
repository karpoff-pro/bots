import { createReducer } from 'redux-act';

import {
  fetchUserStartAction,
  fetchUserSuccessAction,
  fetchUserFailureAction,
} from '../actions/user-get';

const initialState = {
  data: null,
  isLoading: false,
  isProcessing: false,
};

const fetchSuccessReducer = (state, payload) => ({
  ...state,
  isLoading: false,
  data: payload.result,
});

const fetchStartReducer = (state) => ({
  ...state,
  isLoading: true,
});

const fetchFailureReducer = (state) => ({
  ...state,
  isLoading: false,
});

export default createReducer({
  [fetchUserStartAction]: fetchStartReducer,
  [fetchUserSuccessAction]: fetchSuccessReducer,
  [fetchUserFailureAction]: fetchFailureReducer,
}, initialState);
