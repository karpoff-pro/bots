import { createReducer } from 'redux-act';

import { setAdminAction } from '../actions/settings';

const initialState = {
  isAdmin: true,
};

const setAdminReducer = (state, isAdmin) => ({
  ...state,
  isAdmin: Boolean(isAdmin),
});

export default createReducer({
  [setAdminAction]: setAdminReducer,
}, initialState);
