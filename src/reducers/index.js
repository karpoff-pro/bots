import { reducer as formReducer } from 'redux-form';

import settings from './settings';
import list from './user-list';
import item from './user-item';
import info from './info';

export default {
  form: formReducer,
  settings,
  list,
  item,
  info,
};
