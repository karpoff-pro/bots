import { createReducer } from 'redux-act';
import { showInfoAction, hideInfoAction } from '../actions/info';

const initialState = {
  message: '',
};

const showInfoReducer = (state, message) => ({
  ...state,
  message,
});

const clearInfoReducer = (state) => ({
  ...state,
  message: '',
});

export default createReducer({
  [showInfoAction]: showInfoReducer,
  [hideInfoAction]: clearInfoReducer,
}, initialState);
